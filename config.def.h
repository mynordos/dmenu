/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar         = 1;         /* -b  option; if 0, dmenu appears at bottom */
static int fuzzy          = 1;         /* -F  option; if 0, dmenu doesn't use fuzzy matching */
static const char *prompt = NULL;      /* -p  option; prompt to the left of input field */
static int colorprompt    = 0;         /*             if 1, prompt uses SchemeSel, otherwise SchemeNorm */

/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"monospace:size=10"
};

static const char *colors[SchemeLast][2] = {
	/*     fg          bg       */
	[SchemeNorm] = { "#e5e9f0", "#3b4252" },
	[SchemeSel] =  { "#3b4252", "#88c0d0" },
	[SchemeOut] =  { "#b48ead", "#3b4252" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 2;
